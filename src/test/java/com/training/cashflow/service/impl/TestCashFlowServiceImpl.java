package com.training.cashflow.service.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.cashflow.dto.CashFlow;
import com.training.cashflow.dto.CashFlowDto;
import com.training.cashflow.dto.ResultDto;

@ExtendWith(SpringExtension.class)
public class TestCashFlowServiceImpl {
	@InjectMocks
	CashFlowServiceImpl cashFlowServiceImpl;
//	@Test
//	void testFindNegativeBalanceDate() {
//		List<CashFlow> cashFlows=Arrays.asList(
//				new CashFlow("2023-03-09","daily", 10, "debit")
//				);
//		CashFlowDto cashFlowDto=new CashFlowDto(100, "2023-03-10", cashFlows);
//		ResultDto resultDto =cashFlowServiceImpl.findNegativeBalanceDate(cashFlowDto);
//		assertEquals(LocalDate.parse("2023-03-09"), resultDto.getDate());
//	}
//	
//	@Test
//	void testAlreadyNegativeBalance() {
//		List<CashFlow> cashFlows=Arrays.asList(
//				new CashFlow("2023-03-09","daily", 10, "debit")
//				);
//		CashFlowDto cashFlowDto=new CashFlowDto(-10, "2023-03-10", cashFlows);
//		ResultDto resultDto =cashFlowServiceImpl.findNegativeBalanceDate(cashFlowDto);
//		assertEquals(LocalDate.parse("2023-03-09"), resultDto.getDate());
//	}
	
	@Test
	void test() {
		List<CashFlow> cashFlows=Arrays.asList(
				new CashFlow("2023-03-09","daily", 10, "debit")
				);
		CashFlowDto cashFlowDto=new CashFlowDto(100, "2023-03-10", cashFlows);
		ResultDto resultDto =cashFlowServiceImpl.findNegativeBalanceDate(cashFlowDto);
		assertFalse(false);
	}

	

	
	
	
}
