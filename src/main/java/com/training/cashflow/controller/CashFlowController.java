package com.training.cashflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.cashflow.dto.CashFlowDto;
import com.training.cashflow.dto.ResultDto;
import com.training.cashflow.service.CashFlowService;

import jakarta.validation.Valid;



@RestController
@RequestMapping("/cashflow")
public class CashFlowController {
	@Autowired
	private CashFlowService cashFlowService;
	@PostMapping
	public ResponseEntity<ResultDto> findCashFlow(@RequestBody @Valid CashFlowDto cashFlowDto){

		return ResponseEntity.ok(cashFlowService.findNegativeBalanceDate(cashFlowDto));
	}
	
	
	
	

}
