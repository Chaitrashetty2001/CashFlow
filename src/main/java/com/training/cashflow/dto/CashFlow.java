package com.training.cashflow.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;

public class CashFlow {
	@Pattern(regexp="^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$",message = "Date must be in yyyy-mm-dd")
	private String startDate;
	@Pattern(regexp = "\\b(?:daily|monthly|weekly)\\b",message="Frequency must be either among daily,weekly or monthly")
	private String frequency;
	@Min(1)
	private double amount;
	@Pattern(regexp = "\\b(?:credit|debit)\\b",message = "Type must be either among credit or debit")
	private String type;
	public String getStartDate() {
		return startDate;
	} 
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public CashFlow(String startDate, String frequency, double amount, String type) {
		super();
		this.startDate = startDate;
		this.frequency = frequency;
		this.amount = amount;
		this.type = type;
	}
	public CashFlow() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
