package com.training.cashflow.dto;

import java.time.LocalDate;

public class ResultDto {
	private LocalDate date;

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date; 
	}

	public ResultDto(LocalDate date) {
		super();
		this.date = date;
	}

	public ResultDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
