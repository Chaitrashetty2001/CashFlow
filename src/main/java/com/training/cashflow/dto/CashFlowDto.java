package com.training.cashflow.dto;

import java.util.List;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

public class CashFlowDto {
	@Min(1)
	private double balance;
	@Pattern(regexp="^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$",message = "Date must in yyyy-mm-dd")
	private String asPerDate;
	@Valid
	List<CashFlow> cashFlow;
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getAsPerDate() {
		return asPerDate;
	}
	public void setAsPerDate(String asPerDate) {
		this.asPerDate = asPerDate;
	}
	public List<CashFlow> getCashFlow() {
		return cashFlow;
	}
	public void setCashFlow(List<CashFlow> cashFlow) {
		this.cashFlow = cashFlow;
	}
	public CashFlowDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CashFlowDto(double balance, String asPerDate, List<CashFlow> cashFlow) {
		super();
		this.balance = balance;
		this.asPerDate = asPerDate;
		this.cashFlow = cashFlow;
	}
	

}
