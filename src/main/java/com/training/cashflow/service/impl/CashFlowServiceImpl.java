package com.training.cashflow.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.training.cashflow.dto.CashFlow;
import com.training.cashflow.dto.CashFlowDto;
import com.training.cashflow.dto.ResultDto;
import com.training.cashflow.service.CashFlowService;

@Service
public class CashFlowServiceImpl implements CashFlowService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Override
    public ResultDto findNegativeBalanceDate(CashFlowDto cashFlowDto) {
        List<CashFlow> cashFlows = cashFlowDto.getCashFlow();
        LocalDate asPerDate = LocalDate.parse(cashFlowDto.getAsPerDate());
        double balance = cashFlowDto.getBalance();

        LocalDate earliestDate = cashFlows.stream()
                .map(CashFlow::getStartDate)
                .map(LocalDate::parse)
                .min(LocalDate::compareTo)
                .orElse(asPerDate);

        LocalDate beginDate = asPerDate.isAfter(earliestDate) ? asPerDate : earliestDate;

        while (balance >= 0) {
            for (CashFlow cf : cashFlows) {
                LocalDate startDate = LocalDate.parse(cf.getStartDate());
                if (startDate.isBefore(beginDate)) {
                    cf.setStartDate(beginDate.toString());
                }
                if (startDate.equals(beginDate)) {
                    balance += cf.getType().equals("credit") ? cf.getAmount() : -cf.getAmount();
                    logger.info("Balance " + balance + "  date:" + beginDate + " " + cf.getFrequency());
                    cf.setStartDate(getNextDate(startDate, cf.getFrequency()).toString());
                }
            }
            beginDate = beginDate.plusDays(1);
        }

        System.out.println("Result: " + beginDate.minusDays(1));
        return new ResultDto(beginDate.minusDays(1));
    }

    
     LocalDate getNextDate(LocalDate startDate, String frequency) {
        return switch (frequency) {
            case "daily" -> startDate.plusDays(1);
            case "weekly" -> startDate.plusWeeks(1);
            case "monthly" -> startDate.plusMonths(1);
            default -> startDate;
        };
    }
}
	
	
	
	
	
	
	
