package com.training.cashflow.service;

import com.training.cashflow.dto.CashFlowDto;
import com.training.cashflow.dto.ResultDto;

public interface CashFlowService {
	ResultDto findNegativeBalanceDate(CashFlowDto cashFlowDto);

}
